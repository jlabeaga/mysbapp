package com.mydomain.mysbapp.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mydomain.mysbapp.domain.User;

/**
 * Repository to access {@link User} instances.
 * 
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
	List<User> findByUsername(String username);	

}