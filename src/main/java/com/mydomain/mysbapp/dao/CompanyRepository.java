package com.mydomain.mysbapp.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mydomain.mysbapp.domain.Company;
import com.mydomain.mysbapp.domain.User;

/**
 * Repository to access {@link Company} instances.
 * 
 */
public interface CompanyRepository extends CrudRepository<Company, Long> {
	
	List<Company> findByName(String name);	

}