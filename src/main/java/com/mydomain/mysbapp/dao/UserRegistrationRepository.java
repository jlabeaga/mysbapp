package com.mydomain.mysbapp.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mydomain.mysbapp.domain.UserRegistration;

/**
 * Repository to access {@link UserRegistration} instances.
 * 
 */
public interface UserRegistrationRepository extends CrudRepository<UserRegistration, Long> {

	List<UserRegistration> findByUsername(String username);

	List<UserRegistration> findByToken(String token);

}