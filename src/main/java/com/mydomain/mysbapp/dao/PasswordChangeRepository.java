package com.mydomain.mysbapp.dao;

import org.springframework.data.repository.CrudRepository;

import com.mydomain.mysbapp.domain.PasswordChange;

/**
 * Repository to access {@link PasswordChange} instances.
 * 
 */
public interface PasswordChangeRepository extends CrudRepository<PasswordChange, Long> {

}