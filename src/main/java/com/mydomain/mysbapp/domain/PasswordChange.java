package com.mydomain.mysbapp.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="PASSWORD_CHANGE")
public class PasswordChange {

	public static final String STATUS_PENDING = "PENDING";
	public static final String STATUS_CONFIRMED = "CONFIRMED";
	
	// unique identifier for the change password process
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	Long id;
	
	// PENDING, CONFIRMED
	String status;
	
	String username; // username = email in case of type = "EMAIL"
	
	String password; // MD5 encripted
	
	Date requestDate;
	
	Date confirmationDate;
	
}
