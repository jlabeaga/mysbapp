package com.mydomain.mysbapp.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="USER_REGISTRATION")
public class UserRegistration {

	public static final String STATUS_PENDING = "PENDING";
	public static final String STATUS_CONFIRMED = "CONFIRMED";
	
	public static final String TYPE_EMAIL = "EMAIL";
	public static final String TYPE_FACEBOOK = "FACEBOOK";
	public static final String TYPE_TWITTER = "TWITTER";
	public static final String TYPE_LINKEDIN = "LINKEDIN";
	
	// unique identifier for the change password process
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	// for the moment only EMAIL type is implemented
	private String type = TYPE_EMAIL;
	
	// Token to identify this user registration process via URL
	private String token;

	private String username; // username = email in case of type = "EMAIL"
	
	private String password; // MD5 encripted
	
	// PENDING, CONFIRMED, CANCELED. On creation PENDING status is assumed
	private String status = STATUS_PENDING;
	
	//  On creation sysdate is assumed for the request date
	private Date requestDate = new Date();
	
	private Date confirmationDate;
	
	public String toString() {
		return String.format("UserRegistration(id=%d, name=%s, token=%s, type=%s, requestDate=%s, confirmationDate=%s)", id, username, token, type, requestDate, confirmationDate);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}
	
}
