package com.mydomain.mysbapp.service;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mydomain.mysbapp.dao.PasswordChangeRepository;
import com.mydomain.mysbapp.dao.UserRegistrationRepository;
import com.mydomain.mysbapp.dao.UserRepository;
import com.mydomain.mysbapp.domain.User;
import com.mydomain.mysbapp.domain.UserRegistration;
import com.mydomain.mysbapp.utils.TokenProcessUtility;

@Service
public class UserService implements IUserService {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	UserRegistrationRepository userRegistrationRepository;
	
	@Autowired
	PasswordChangeRepository passwordChangeRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	TokenProcessUtility tokenProcessUtility;
	
	@Override
	public User findUserByUsername(String username) {

		logger.debug("findUserByUsername called for username = " + username);

		User user = null;
		List<User> list = userRepository.findByUsername(username);
		if( !list.isEmpty() ) {
			user = list.get(0);
		}
		return user;
	}
	
	@Override
	public String requestUserRegistration(String username, String password)
			throws  UserRegUserAlreadyExistsException,
					UserRegUsernamePasswordNotCompliantException {

		logger.debug("requestUserRegistration called for username = " + username);
		
		// if user already exist throw an error
		
		User user = findUserByUsername(username);
		if( user != null ) {
			throw new UserRegUserAlreadyExistsException();
		}
		
		// if a previous user registration process already exist delete it
		// (a new user registration request will be created below)
		
		UserRegistration userRegistration = null;
		List<UserRegistration> listUR = userRegistrationRepository.findByUsername(username);
		if( !listUR.isEmpty() ) {
			userRegistration = listUR.get(0);
			if( userRegistration != null ) {
				userRegistrationRepository.delete(userRegistration);
			}
		}
		
		// reject non-compliant username or password
		
		userRegistration = new UserRegistration();
		userRegistration.setUsername(username);
		userRegistration.setPassword(password);
		validateUserRegistration(userRegistration);
		
		// create the user registration request and return the confirmation token
		
		String token = tokenProcessUtility.generateToken();
		userRegistration.setToken( token );
		userRegistrationRepository.save(userRegistration);
		
		logger.debug("requestUserRegistration saved userRegistration = " + userRegistration);
		
		return token;
		
	}

	/**
	 * Validate the user data: username and password
	 * 
	 * @param userRegistration data to be validated
	 * @throws UserRegUsernamePasswordNotCompliantException
	 */
	private void validateUserRegistration(UserRegistration userRegistration)
			throws UserRegUsernamePasswordNotCompliantException {
		
		if( userRegistration == null ) {
			throw new UserRegUsernamePasswordNotCompliantException();
			// pending validations for username and password
		}
	}
	
	
	@Override
	public User confirmUserRegistration(String token) throws TokenNotFoundException {

		logger.debug("confirmUserRegistration called for token = " + token);

		List<UserRegistration> list = userRegistrationRepository.findByToken(token);
		if( list.isEmpty() ) {
			throw new TokenNotFoundException();
		}
		
		UserRegistration userRegistration = list.get(0);
		
		User user = new User();
		user.setUsername( userRegistration.getUsername() );
		user.setPassword( userRegistration.getPassword() );
		user.setActive( true );
		user.setNickname( userRegistration.getUsername() );
		user.setActive( true );
		
		userRepository.save( user );
		
		userRegistration.setStatus( UserRegistration.STATUS_CONFIRMED );
		userRegistration.setConfirmationDate( new Date() );
		userRegistrationRepository.save( userRegistration );
		
		return user;
	}
	

	@Override
	public User deactivateUser(String username) throws UserRegUserDoesNotExistException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User activateUser(String username) throws UserRegUserDoesNotExistException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String requestPasswordChange(String username) throws UserRegUserDoesNotExistException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User confirmPasswordChange(String token)
			throws UserRegUsernamePasswordNotCompliantException, TokenNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User changePassword(String username, String newPassword)
			throws UserRegUsernamePasswordNotCompliantException, UserRegUserDoesNotExistException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User login(String username, String password) throws UserRegUsernamePasswordInvalidException {
		// TODO Auto-generated method stub
		return null;
	}

}
