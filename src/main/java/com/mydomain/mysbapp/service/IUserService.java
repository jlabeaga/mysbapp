package com.mydomain.mysbapp.service;

import com.mydomain.mysbapp.domain.User;

public interface IUserService {

	/**
	 * Find a user by its username
	 * 
	 * @param username to identify the user
	 * @return user found or null if no user is found with this username
	 */
	public User findUserByUsername(String username);
	
	/**
	 * Request a user registration and return an operation token to be emailed to user for confirmation
	 * 
	 * Only if the registration type is
	 *  
	 * @param username email
	 * @param password to be encrypted
	 * @return userRegistrationToken that will be emailed to the user for him to confirm registration
	 * @throws UserRegUserAlreadyExistsException in case there is alreday a registered user with this username
	 * @throws UserRegUsernamePasswordNotCompliantException in case username or password do not comply with validation
	 */
	String requestUserRegistration( String username, String password )
		throws UserRegUserAlreadyExistsException, UserRegUsernamePasswordNotCompliantException;

	/**
	 * Confirm a user registration and create the user in the corresponding table
	 * 
	 * @param token identifies the registration process initiated by the user
	 * @return registered user
	 * @throws TokenNotFoundException in case token is not found
	 */
	User confirmUserRegistration( String token )
		throws TokenNotFoundException;
	
	/**
	 * Deactivate an active user
	 * 
	 * @param username identifies the user
	 * @return deactivated user
	 * @throws UserRegUserDoesNotExistException in case username is not found
	 */
	User deactivateUser( String username )
		throws UserRegUserDoesNotExistException;
	 
	/**
	 * Activate an inactive user
	 * 
	 * @param username identifies the user
	 * @return activated user
	 * @throws UserRegUserDoesNotExistException in case username is not found
	 */
	
	User activateUser( String username )
		throws UserRegUserDoesNotExistException;
	 
	/**
	 * Request a password change and return an operation token to be emailed to user
	 * 
	 * If the user is inactive he will be activated when the password change is confirmed
	 *  
	 * @param username email for user identification
	 * @return passwordChangeToken that will be emailed to the user for him to confirm registration
	 * @throws UserRegUserDoesNotExistException in case username is not found
	 */
	String requestPasswordChange( String username )
			throws UserRegUserDoesNotExistException;

	/**
	 * Change the user password using the passwordChangeToken generated
	 * 
	 * If the user was inactive he will be activated automatically
	 *  
	 * @param token identifies the registration process initiated by the user
	 * @return user
	 * @throws UserRegUsernamePasswordNotCompliantException in case username or password do not comply with validation
	 * @throws TokenNotFoundException in case token is not found
	 */
	User confirmPasswordChange( String token )
		throws UserRegUsernamePasswordNotCompliantException, TokenNotFoundException;
	
	/**
	 * Change the user password directly without any passwordChangeToken
	 * 
	 * @param username email for user identification
	 * @param newPassword encrypted
	 * @return user
	 * @throws UserRegUsernamePasswordNotCompliantException in case username or password do not comply with validation
	 * @throws UserRegUserDoesNotExistException in case username is not found
	 */
	User changePassword( String username, String newPassword )
			throws UserRegUsernamePasswordNotCompliantException, UserRegUserDoesNotExistException;
	
	/**
	 * Attempt a user login with the username and password
	 * 
	 * @param username id for user identification
	 * @param password to be encrypted
	 * @return successfully logged in user
	 * @throws UserRegUsernamePasswordInvalidException in case username and/or password are not found
	 */
	User login( String username, String password )
		throws UserRegUsernamePasswordInvalidException;
		
}
