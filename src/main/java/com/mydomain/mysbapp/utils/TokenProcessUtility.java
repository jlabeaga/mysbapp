package com.mydomain.mysbapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Operation/Process handled via user-confirmed emails tokens in the URL
 * 
 * Each element represents an operation (user registration via email,
 * password reset, etc) that needs to be confirmed by the user. by means of
 * a token  sent by email. The token identifies the operation to be confirmed.
 */
@Service
public class TokenProcessUtility {

	Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	private SimpleDateFormat sdfToken = new SimpleDateFormat("yyyyMMddhhmmssSSS");
	
	private Random randomGenerator = new Random();
	
	/**
	 * Generate a random token to be sent on URLs for email confirmation purposes
	 * 
	 * The user will receive an email to confirm the operation, inside the email there will be a
	 * confirmation UR. When the user clicks on the link the operation will be confirmed.
	 * 
	 * The token is URL-friendly to prevent URL parse errors
	 * 
	 * @return randomly generated token
	 */
	public String generateToken() {

		// generate a string with the format: yyyyMMddhhmmssSSSrrrrrrrrrrrr
		// where rrrrrrrrrrrr is a randomly generated integer <= Integer.MAX_VALUE
		// the prefix yyyyMMddhhmmssSSS is the creation timestamp that allow us to easily sort tokens
		String token = sdfToken.format( new Date() ) + Integer.toString(randomGenerator.nextInt(Integer.MAX_VALUE));
		logger.debug("generated token: " + token);
		return token;
		
	}
	
}
