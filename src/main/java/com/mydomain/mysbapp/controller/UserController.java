package com.mydomain.mysbapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mydomain.mysbapp.domain.User;
import com.mydomain.mysbapp.service.IUserService;
import com.mydomain.mysbapp.service.TokenNotFoundException;
import com.mydomain.mysbapp.service.UserRegUserAlreadyExistsException;
import com.mydomain.mysbapp.service.UserRegUsernamePasswordNotCompliantException;

@Controller
public class UserController {

	Logger logger = LoggerFactory.getLogger(this.getClass().getName());

	@Autowired
	IUserService userService;

	@RequestMapping("/user_registration_edit")
	public String userRegistrationEdit(Model model) {
		logger.debug("userRegistrationEdit called");
		model.addAttribute("msg", "Please enter user registration info");
		return "user/user_registration_edit";
	}

	@RequestMapping("/user_registration_request")
	public String userRegistrationRequest(@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password, Model model) {

		logger.debug("userRegistrationRequest called for email = " + email);

		String token;

		try {

			token = userService.requestUserRegistration(email, password);
			logger.debug("userRegistrationToken generated: " + token);
			model.addAttribute("token", token);
			return "user/user_registration_requested";

		} catch (UserRegUserAlreadyExistsException e) {

			model.addAttribute("msg", "Error: email " + email + " already exists.");
			return "user/user_registration_edit";

		} catch (UserRegUsernamePasswordNotCompliantException e) {

			model.addAttribute("msg", "Error: username or password are non-compliant with the rules.");
			return "user/user_registration_edit";
		}

	}

	@RequestMapping("/user_registration_confirm")
	public String userRegistrationConfirm(@RequestParam("token") String token, Model model) {

		logger.debug("userRegistrationConfirm called with token = " + token);

		try {
			User user = userService.confirmUserRegistration(token);
			model.addAttribute("username", user.getUsername());
			return "user/user_registration_confirmed";
		} catch (TokenNotFoundException e) {
			model.addAttribute("msg", "Error: invalid token.");
			return "user/user_registration_edit";
		}

	}

	@RequestMapping("/login_goto")
	public String gotoLogin(@RequestParam(value = "username", required = false) String username, Model model) {

		logger.debug("gotoLogin called with username = " + username);

		model.addAttribute("username", username);
		return "user/login";
	}

	@RequestMapping("/login")
	public String login(@RequestParam("username") String username, @RequestParam("password") String password,
			Model model) {

		logger.debug("login called for username = " + username);

		throw new RuntimeException("login operation not yet implemented");

	}

}
