package com.mydomain;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.mydomain.mysbapp.dao.CompanyRepository;
import com.mydomain.mysbapp.dao.UserRepository;
import com.mydomain.mysbapp.domain.Company;
import com.mydomain.mysbapp.domain.User;

@SpringBootApplication
public class MysbappApplication {

	@Bean
	InitializingBean insertTestData(UserRepository userRepo, CompanyRepository companyRepo){
		return () -> {
			
			Company company1 = companyRepo.save(new Company("company1"));
			Company company2 = companyRepo.save(new Company("company2"));

			userRepo.save(new User("user1@hotmail.com", "password", true, "user1", company1));
			userRepo.save(new User("user2@hotmail.com", "password", true, "user2", company1));
			userRepo.save(new User("user3@hotmail.com", "password", true, "user3", company2));
			
		};
	}
	
	public static void main(String[] args) {
		SpringApplication.run(MysbappApplication.class, args);
	}
}
