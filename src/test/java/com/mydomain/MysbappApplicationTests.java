package com.mydomain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mydomain.mysbapp.utils.TokenProcessUtility;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MysbappApplicationTests {

	@Autowired
	TokenProcessUtility tokenProcessUtility;
	
	@Test
	public void contextLoads() {
	}

	@Test
	public void dummyTest() {
		
		System.out.println("Testing assertEquals");
		Integer int1 = new Integer(1);
		Integer int2 = new Integer(1);
		assertEquals(int1, int2);
		
		//throw new RuntimeException("PROVOKED DUMMY EXCEPTION");
	}

	@Test
	public void generateToken_ShouldNotBeNull() {
		
		System.out.println("Testing generateToken");
		String token = tokenProcessUtility.generateToken();
		assertNotNull(token);
	}

}
